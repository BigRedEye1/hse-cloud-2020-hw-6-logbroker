#!/usr/bin/env python3

import os
import sys
from typing import *
from jinja2 import Template
from pathlib import Path
import logging as log
import subprocess as sp

ROOT = Path(os.path.dirname(os.path.realpath(__file__)))

class CalledProcessError(Exception):
    def __init__(self, code: int, stderr: str):
        self.code = code
        self.stderr = stderr
        self.message = f'Called process failed with code: {code}, stderr: {stderr}'
        super().__init__(self.message)


def run(args: list[str], cwd: Optional[str]=None, interactive: bool=False) -> str:
    log.info(f'Running command "{args}"')
    kwargs = {'stderr': sys.stderr, 'stdout': sys.stdout} if interactive else {'capture_output': True}
    res = sp.run(args, shell=True, cwd=cwd, encoding='utf-8', **kwargs)
    if res.returncode != 0:
        log.error('Called process failed with code %d and stderr %s', res.returncode, res.stderr)
        raise CalledProcessError(res.returncode, res.stderr)
    log.info('Called process finished with code 0 and stderr %s', res.stderr)
    return res.stdout


class Env:
    ARGS = [
        ('cloud', 'YC_CLOUD_ID', 'Id of the cloud', str),
        ('folder', 'YC_FOLDER_ID', 'Id of the folder', str),
        ('token', 'YC_TOKEN', 'OAUTH token', str),
        ('cr', 'YC_CR', 'Container registry name', str),
        ('dryrun', 'DRY_RUN', 'Do not apply changes', bool),
        ('replication', 'CH_REPLICATION', 'Number of clickhouse replicas', int),
    ]

    def __init__(self):
        self.env = os.environ
        self.args = {}
        for id, key, help, type in Env.ARGS:
            value = type(self._get_param(key, help))
            setattr(self, id, value)
            self.args[key] = value

        self._patch_environ()

    def _get_param(self, key: str, help: str) -> str:
        if key in self.env:
            return self.env[key]
        else:
            return input(f'Enter value for ${key} ({help}): ')

    def _patch_environ(self) -> None:
        for key, value in self.args.items():
            self.env[key] = str(value)
        self.env['TF_VAR_replication'] = str(self.replication)
        self.env['TF_VAR_folder_id'] = str(self.folder)


class DockerImage:
    def __init__(self, image_id: Optional[str]=None, tag: Optional[str]=None):
        assert image_id or tag
        if tag is not None:
            self._tag = tag
            self._pushed = True
        elif image_id is not None:
            self._id = image_id
            self._tag = None
            self._pushed = False

    def push(self) -> None:
        assert self.is_tagged()
        if self._pushed:
            return
        self._pushed = True
        log.info(f'Pushing image {self._tag}')
        run(f'docker push {self._tag}')

    def tag(self, tag: str):
        assert not self.is_tagged()
        self._tag = tag
        log.info(f'Tagging image {self._id} with tag {self._tag}')
        run(f'docker tag {self._id} {self._tag}')

    def is_tagged(self) -> bool:
        return bool(self._tag)

    def get_tag(self) -> str:
        assert self.is_tagged()
        return self._tag


class Docker:
    def build(self, cwd: str='.') -> DockerImage:
        log.info('Building docker image from directory %s' % os.path.realpath(cwd))
        image = run(f'docker build -q {cwd}').strip()
        log.info('Done building docker image %s' % image)
        return DockerImage(image_id=image)

    def load(self, tag: str) -> DockerImage:
        log.info('Using docker image %s' % tag)
        return DockerImage(tag=tag)

def _generate_config(src: Path, dst: Path, ctx: dict) -> None:
    tmpl = _load_jinja_template(src)
    config = tmpl.render(ctx)
    with open(dst, 'w') as f:
        f.write(config)

def _load_jinja_template(path: os.PathLike) -> Template:
    return Template(open(path).read())


class Container:
    def __init__(self, env: Env):
        self._env = env
        self._image = None

    def build(self) -> None:
        self._generate_configs()
        self._push_image()

    def _push_image(self):
        image = self._make_image()
        if not image.is_tagged():
            image.tag(self._make_docker_tag())
        if not self._env.dryrun:
            image.push()
        else:
            log.info('Skipped image uploading')
        self._image = image

    def _make_docker_tag(self) -> str:
        if self.name() == 'logbroker':
            return f'cr.yandex/{self._env.cr}/{self.name()}:0.1.0'
        else:
            return f'cr.yandex/{self._env.cr}/{self.name()}:next'

    def image(self) -> DockerImage:
        return self._image

    # Unimplemented
    def _generate_configs(self) -> None:
        pass

    # Unimplemented
    def _make_image(self) -> DockerImage:
        raise Exception('Unimplemented')

    # Unimplemented
    def name(self) -> str:
        raise Exception('Unimplemented')

    # Unimplemented
    def env(self) -> dict[str, str]:
        return {}


if False:
    class NginxService(Service):
        def name(self) -> str:
            return 'nginx'

        def _generate_configs(self) -> None:
            ctx = {
                'backends': [f'healthcheck-{i}' for i in range(self._env.replication)],
            }
            _generate_config(
                src=ROOT / 'nginx' / 'nginx.conf.jinja2',
                dst=ROOT / 'nginx' / 'nginx.conf.jinja2.generated',
                ctx=ctx)

        def _make_image(self) -> DockerImage:
            return Docker().build(cwd='nginx')


    class PostgresService(Service):
        def name(self) -> str:
            return 'postgres'

        def _make_image(self) -> DockerImage:
            return DockerImage('postgres:13')

        def _make_env(self) -> dict[str, str]:
            return {
                'POSTGRES_PASSWORD': 'aefac2e2d9fccd1',
                'POSTGRES_USER': 'healthcheck',
            }

    class PostgresService(Service):
        def name(self) -> str:
            return 'postgres'

        def _make_image(self) -> DockerImage:
            return DockerImage('postgres:13')

        def _make_env(self) -> dict[str, str]:
            return {
                'POSTGRES_PASSWORD': 'aefac2e2d9fccd1',
                'POSTGRES_USER': 'healthcheck',
            }


    class HealthcheckService(Service):
        def name(self) -> str:
            return 'healthcheck'

        def _make_image(self) -> DockerImage:
            return Docker().build(cwd='app')

        def _make_env(self) -> dict[str, str]:
            return {
                'NODE_DATABASE_URL': 'postgres://healthcheck:aefac2e2d9fccd1@postgres/postgres',
                'NODE_BIND_ADDRESS': '0.0.0.0:80',
            }

class ClickHouse(Container):
    def name(self) -> str:
        return 'clickhouse'

    def _generate_configs(self) -> None:
        ctx = {
            'replicas': [f'clickhouse-{i}' for i in range(self._env.replication)],
        }
        _generate_config(
            src=ROOT / 'clickhouse' / 'config.xml.jinja2',
            dst=ROOT / 'clickhouse' / 'config.xml',
            ctx=ctx)

    def _make_image(self) -> DockerImage:
        return Docker().build(cwd='clickhouse')

class ZooKeeper(Container):
    def name(self) -> str:
        return 'zookeeper'

    def _make_image(self) -> DockerImage:
        return Docker().load(tag='zookeeper')

class LogBroker(Container):
    def name(self) -> str:
        return 'logbroker'

    def _make_image(self) -> DockerImage:
        return Docker().build(cwd='logbroker/daemon')

    def _make_env(self) -> dict[str, str]:
        return {
            'LOGBROKER_BIND_ADDRESS': '0.0.0.0:8000',
            'LOGBROKER_CH_REPLICAS': 'clickhouse-0:8123,clickhouse-1:8123,clickhouse-2:8123',
            'LOGBROKER_DATABASE_PATH': '/var/run/logbroker/db/db.sqlite',
            'LOGBROKER_STORAGE_ROOT': '/var/run/logbroker/logs',
        }

class Pod:
    def __init__(self, name: str, containers: list[Container]):
        self._name = name
        self._containers = containers

    def build(self) -> None:
        for container in self._containers:
            container.build()
        self._generate_docker_declaration()

    def name(self) -> str:
        return self._name

    def _generate_docker_declaration(self) -> None:
        containers = []
        for container in self._containers:
            containers.append({
                'image': container.image().get_tag(),
                'env': container.env(),
                'name': container.name(),
            })

        _generate_config(
            src=ROOT / 'terraform' / 'docker' / 'template.jinja2',
            dst=ROOT / 'terraform' / 'docker' / (self._name + '.generated.yaml'),
            ctx={'containers': containers}
        )


def load_env() -> Env:
    return Env()


def main() -> None:
    log.basicConfig(level=log.INFO, format='%(asctime)s %(levelname)s %(message)s')
    env = load_env()

    pods = [
        Pod(name='clickhouse', containers=[ClickHouse(env), ZooKeeper(env)]),
        Pod(name='logbroker', containers=[LogBroker(env)]),
    ]
    for pod in pods:
        log.info('Building pod %s' % pod.name())
        pod.build()

    log.info('Running terraform plan')
    print(run('terraform plan', cwd=ROOT / 'terraform'))

    if not env.dryrun:
        log.info('Running terraform apply')
        print(run('terraform apply -auto-approve', cwd=ROOT / 'terraform', interactive=True))
    else:
        log.info('Skipped terraform apply')

if __name__ == '__main__':
    main()
