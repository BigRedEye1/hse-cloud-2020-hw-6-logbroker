#!/usr/bin/env python3

import sys
import json
import argparse
from logbroker import LogBroker

def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('logbroker', type=str, help='logbroker url')
    parser.add_argument('table', type=str, help='logbroker table')
    args = parser.parse_args()

    logger = LogBroker(args.logbroker, args.table)

    for line in sys.stdin:
        res = json.loads(line)
        try:
            logger.log(res)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main()
