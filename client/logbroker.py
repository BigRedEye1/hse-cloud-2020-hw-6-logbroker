import requests

class LogBroker:
    def __init__(self, url: str, table: str) -> None:
        self._url = url
        self._table = table

    def log(self, context: dict) -> None:
        res = requests.post(self._url + '/write_log', json=[{'table_name': self._table, 'rows': context}])
        if res.status_code != 200:
            raise Exception(f'Failed to send logs: {res.json()}')
