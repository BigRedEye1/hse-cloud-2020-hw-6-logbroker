resource "yandex_lb_network_load_balancer" "balancer" {
  name = "balancer"

  listener {
    name = "redirect"
    port = 80
    target_port = 8000
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.logbroker.load_balancer[0].target_group_id

    healthcheck {
      name = "http"
      http_options {
        port = 8000
        path = "/healthcheck"
      }
    }
  }
}

