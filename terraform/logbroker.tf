resource "yandex_compute_instance" "logbroker-tmpl" {
  name = "logbroker-tmpl"
  hostname = "logbroker-tmpl"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }

  allow_stopping_for_update = true

  service_account_id = yandex_iam_service_account.cr-accessor.id

  network_interface {
    subnet_id = yandex_vpc_subnet.private-subnet.id
    nat       = false
  }

  metadata = {
    ssh-keys = "ubuntu:${file("/home/sergey/.ssh/id_yandex_cloud.pub")}"
    docker-compose = file("${path.module}/docker/logbroker-compose.yaml")
  }
}

resource "yandex_compute_instance_group" "logbroker" {
  name                = "logbroker"
  service_account_id  = yandex_iam_service_account.scaler-sa.id
  deletion_protection = false
  depends_on          = [yandex_iam_service_account.scaler-sa]

  instance_template {
    name = "logbroker-{instance.index}"
    hostname = "logbroker-{instance.index}"

    platform_id = "standard-v2"

    resources {
      cores  = 2
      memory = 2
    }

    boot_disk {
      initialize_params {
        image_id = data.yandex_compute_image.container-optimized-image.id
      }
    }

    service_account_id = yandex_iam_service_account.cr-accessor.id

    network_interface {
      network_id = yandex_vpc_network.main-network.id
      subnet_ids = [yandex_vpc_subnet.private-subnet.id]
      nat       = false
    }

    metadata = {
      ssh-keys = "ubuntu:${file("/home/sergey/.ssh/id_yandex_cloud.pub")}"
      docker-compose = file("${path.module}/docker/logbroker-compose.yaml")
    }

    network_settings {
      type = "STANDARD"
    }
  }

  load_balancer {
    target_group_name = "logbroker-target-group"
  }

  scale_policy {
    auto_scale {
      initial_size = 2
      measurement_duration = 60
      cpu_utilization_target = 50
      max_size = 5
      min_zone_size = 2
    }
  }

  allocation_policy {
    zones = [var.zone]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }
}

