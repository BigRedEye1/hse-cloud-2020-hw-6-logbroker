variable "replication" {}
variable "zone"        {default = "ru-central1-a"}
variable "folder_id"   {default = ""}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.49.0"
    }
  }
}

provider "yandex" {
  zone = var.zone
}

data "yandex_compute_image" "container-optimized-image" {
  family = "container-optimized-image"
}
