resource "yandex_compute_instance" "clickhouse" {
  count = var.replication
  name = "clickhouse-${count.index}"
  hostname = "clickhouse-${count.index}"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }

  allow_stopping_for_update = true

  service_account_id = yandex_iam_service_account.cr-accessor.id

  network_interface {
    subnet_id = yandex_vpc_subnet.private-subnet.id
    nat       = false
  }

  metadata = {
    ssh-keys = "ubuntu:${file("/home/sergey/.ssh/id_yandex_cloud.pub")}"
    docker-compose = file("${path.module}/docker/clickhouse.generated.yaml")
  }
}
