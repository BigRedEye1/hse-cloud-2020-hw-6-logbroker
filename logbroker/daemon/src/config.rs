use crate::prelude::*;

use config;

#[derive(Clone, Debug, serde::Deserialize)]
pub struct Settings {
    pub bind_address: std::net::SocketAddr,
    pub storage_root: std::path::PathBuf,
    pub database_path: std::path::PathBuf,
    pub ch_replicas: String,
    pub ch_user: Option<String>,
    pub ch_password: Option<String>,
}

impl Settings {
    pub fn new() -> Result<Self> {
        let mut s = config::Config::new();
        s.merge(config::Environment::with_prefix("LOGBROKER"))?;
        s.try_into().map_err(|e| anyhow::Error::new(e))
    }
}
