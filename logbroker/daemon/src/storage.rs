use crate::prelude::*;

pub struct Storage {
    root: std::path::PathBuf,
}

impl Storage {
    pub fn new(root: std::path::PathBuf) -> Result<Storage> {
        std::fs::create_dir_all(root)?;
        Ok(Storage { root })
    }
}
