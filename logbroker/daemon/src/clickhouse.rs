use crate::prelude::*;

pub struct ClientBuilder {
    replicas: Vec<String>,
    password: Option<String>,
    user: Option<String>,
}

impl ClientBuilder {
    pub fn new() -> ClientBuilder {
        ClientBuilder {
            replicas: Vec::new(),
            password: None,
            user: None,
        }
    }

    pub fn replica(mut self, addr: String) -> ClientBuilder {
        self.replicas.push(format!("http://{}", addr));
        self
    }

    pub fn user(mut self, value: String) -> ClientBuilder {
        self.user = Some(value);
        self
    }

    pub fn password(mut self, value: String) -> ClientBuilder {
        self.password = Some(value);
        self
    }

    pub fn build(self) -> Client {
        use reqwest::header::{HeaderMap, HeaderValue};

        let mut headers = HeaderMap::new();
        if let Some(user) = self.user {
            headers.insert("X-ClickHouse-User", HeaderValue::from_str(&user).unwrap());
        }
        if let Some(password) = self.password {
            headers.insert(
                "X-ClickHouse-Key",
                HeaderValue::from_str(&password).unwrap(),
            );
        }

        Client {
            replicas: self.replicas,
            client: reqwest::ClientBuilder::new()
                .default_headers(headers)
                .build()
                .unwrap(),
            next_replica: std::sync::Arc::new(std::sync::atomic::AtomicUsize::new(0)),
        }
    }
}

#[derive(Clone)]
pub struct Client {
    replicas: Vec<String>,
    client: reqwest::Client,
    next_replica: std::sync::Arc<std::sync::atomic::AtomicUsize>,
}

impl Client {
    pub async fn query(&self, query: &str, data: Option<Vec<u8>>) -> Result<String> {
        let url = &self.replicas[self.next_replica.fetch_add(1, std::sync::atomic::Ordering::SeqCst) % self.replicas.len()];

        let q = if let Some(data) = data {
            log::debug!("Sending POST request to {}, data: {}", url, String::from_utf8(data.clone()).unwrap());
            self.client.post(url).body(data)
        } else {
            log::debug!("Sending GET request to {}", url);
            self.client.get(url)
        }
        .query(&[("query", &query)]);

        let res = q
            .send()
            .await
            .context("Failed to build clickhouse request")?;

        let status = res.status().clone();
        let text = &res.text().await?;

        match status {
            reqwest::StatusCode::OK => Ok(text.into()),
            _ => Err(anyhow::anyhow!(
                "ClickHouse error; code {}, message: {}",
                status,
                text
            )),
        }
    }
}
