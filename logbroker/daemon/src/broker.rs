use crate::prelude::*;

use crate::api;
use crate::config;
use crate::repo;

use std::sync::Arc;
use tokio::{
    io::AsyncBufReadExt,
    io::AsyncWriteExt,
    io::BufReader,
    sync::{Mutex, MutexGuard},
};

#[async_trait::async_trait]
pub trait Sender: Send + Sync {
    async fn send(&self, entries: Vec<api::LogEntry>) -> Result<()>;
}

pub struct Broker {
    root: std::path::PathBuf,
    storage: Arc<SharedLogsStorage>,

    stop_flag: Arc<std::sync::atomic::AtomicBool>,
    submitter: Option<tokio::task::JoinHandle<()>>,

    sender: Arc<dyn Sender>,
    repo: repo::Repo,
}

impl Broker {
    pub async fn new(conf: &config::Settings, sender: Box<dyn Sender>) -> Result<Broker> {
        let root = &conf.storage_root;
        let db = &conf.database_path;

        std::fs::create_dir_all(&root)?;

        Self::make_locked_logs_ready(root)
            .await
            .context("Failed to make locked logs ready")?;

        let buckets = Arc::new(SharedLogsStorage::new(root).await?);

        let mut broker = Broker {
            root: root.into(),
            storage: buckets,
            submitter: None,
            sender: Arc::from(sender),
            stop_flag: Arc::new(std::sync::atomic::AtomicBool::new(false)),
            repo: repo::Repo::new(db).await?,
        };

        broker.spawn_uploader().await?;

        Ok(broker)
    }

    async fn spawn_uploader(&mut self) -> Result<()> {
        let storage = self.storage.clone();
        let root = self.root.to_path_buf();
        let stop = self.stop_flag.clone();
        let sender = self.sender.clone();
        let repo = self.repo.clone();

        self.submitter = Some(tokio::task::spawn(async move {
            while !stop.load(std::sync::atomic::Ordering::Acquire) {
                if let Err(e) = Self::send_ready_buckets(&root, &*sender, &repo).await {
                    log::error!("Failed to send ready buckets: {:?}", e);
                }

                tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;

                if let Err(e) = storage.rotate().await {
                    log::error!("Failed to rotate buckets: {:?}", e);
                }
            }

            log::info!("Stopping submitter");
        }));

        Ok(())
    }

    async fn make_locked_logs_ready(root: &std::path::Path) -> Result<()> {
        for entry in root.read_dir().expect("Failed to read dir") {
            if let Ok(e) = entry {
                if e.path().extension() != Some(std::ffi::OsStr::new("locked")) {
                    continue;
                }
                std::fs::rename(e.path(), e.path().to_path_buf().with_extension(""))?;
            }
        }
        Ok(())
    }

    async fn send_ready_buckets(
        root: &std::path::Path,
        sender: &dyn Sender,
        repo: &repo::Repo,
    ) -> Result<()> {
        let mut entries: Vec<api::LogEntry> = Vec::new();
        let mut buckets: Vec<(std::path::PathBuf, BucketId, usize)> = Vec::new();

        for entry in root.read_dir().expect("Failed to read dir") {
            if let Ok(e) = entry {
                if e.path().extension() == Some(std::ffi::OsStr::new("locked")) {
                    continue;
                }
                log::debug!("Found bucket {:?}", e.path());
                let (id, count) = Self::load_bucket(&e.path(), &mut entries).await?;
                buckets.push((e.path(), id, count));
            }
        }

        if entries.len() > 0 {
            log::info!(
                "Loaded {} log entries from {} buckets",
                entries.len(),
                buckets.len()
            );
        }

        let status = match sender.send(entries).await {
            Ok(_) => {
                Self::remove_buckets(&buckets)?;
                api::BucketStatus::Success
            }
            Err(e) => {
                log::error!("Failed to upload logs: {:?}", e);
                api::BucketStatus::Failure
            }
        };

        for (_, uuid, count) in buckets {
            if count > 0 {
                repo.update_status(&uuid.to_string(), status).await?;
            } else {
                repo.erase(&uuid.to_string()).await?;
            }
        }

        Ok(())
    }

    async fn load_bucket(
        path: &std::path::Path,
        entries: &mut Vec<api::LogEntry>,
    ) -> Result<(BucketId, usize)> {
        use std::str::FromStr;

        let uuid = uuid::Uuid::from_str(path.file_name().unwrap().to_str().unwrap())?;
        log::debug!("Sending bucket {}", uuid);

        let file = tokio::fs::File::open(path)
            .await
            .context("Failed to open file")?;
        let reader = BufReader::new(file);
        let mut lines = reader.lines();
        let mut count = 0usize;

        while let Some(line) = lines.next_line().await.context("Failed to read line")? {
            log::debug!("Loading line {}", &line);
            entries.push(serde_json::from_str(&line)?);
            count += 1;
        }

        Ok((uuid, count))
    }

    fn remove_buckets(buckets: &[(std::path::PathBuf, BucketId, usize)]) -> Result<()> {
        for (path, _, _) in buckets {
            std::fs::remove_file(path)?;
        }
        Ok(())
    }

    pub async fn submit(&self, entries: Vec<api::LogEntry>) -> Result<uuid::Uuid> {
        let (uuid, count) = self.storage.write(entries).await?;

        if count == 0 {
            self.repo
                .update_status(&uuid.to_string(), api::BucketStatus::Pending)
                .await?;
        }

        Ok(uuid)
    }

    pub async fn status(&self, id: &str) -> Result<api::BucketStatus> {
        self.repo.bucket_status(id).await
    }

    pub async fn stop(&self) {
        self.stop_flag
            .store(true, std::sync::atomic::Ordering::Release);

        if let Err(e) = Self::send_ready_buckets(&self.root, &*self.sender, &self.repo).await {
            log::error!("Failed to send ready buckets: {:?}", e);
        }

        if let Err(e) = self.storage.rotate().await {
            log::error!("Failed to rotate buckets: {:?}", e);
        }
    }
}

struct Bucket {
    uuid: uuid::Uuid,
    file: tokio::fs::File,

    current_path: std::path::PathBuf,
    ready_path: std::path::PathBuf,

    num_entries: usize,
}

impl Bucket {
    pub async fn new(root: &std::path::Path) -> Result<Bucket> {
        std::fs::create_dir_all(&root)?;

        let uuid = uuid::Uuid::new_v4();
        let path = root.join(uuid.to_string() + ".locked");
        let final_path = root.join(uuid.to_string());

        log::debug!("Spawning bucket {}", uuid);
        Ok(Bucket {
            uuid,
            file: tokio::fs::File::create(&path).await?,
            current_path: path,
            ready_path: final_path,
            num_entries: 0,
        })
    }
}

impl Drop for Bucket {
    fn drop(&mut self) {
        log::debug!("Drop bucket {}", self.uuid);
        if let Err(e) = std::fs::rename(&self.current_path, &self.ready_path) {
            log::error!("Failed to finalize bucket {}: {:?}", self.uuid, e);
        }
    }
}

const NUM_SHARDS: usize = 16;

struct SharedLogsStorage {
    root: std::path::PathBuf,
    next: std::sync::atomic::AtomicUsize,
    buckets: Vec<Mutex<Bucket>>,
}

type BucketId = uuid::Uuid;

impl SharedLogsStorage {
    pub async fn new(path: &std::path::Path) -> Result<SharedLogsStorage> {
        let mut buckets: Vec<Mutex<Bucket>> = Vec::new();
        for _ in 0..NUM_SHARDS {
            buckets.push(Mutex::new(Bucket::new(path).await?));
        }

        Ok(SharedLogsStorage {
            root: path.to_path_buf(),
            next: std::sync::atomic::AtomicUsize::new(0),
            buckets,
        })
    }

    pub async fn write(&self, entries: Vec<api::LogEntry>) -> Result<(BucketId, usize)> {
        let mut bucket = self.lock().await;
        let prev_size = bucket.num_entries;

        for entry in entries {
            let serialized = serde_json::to_string(&entry)? + "\n";
            bucket.file.write(serialized.as_bytes()).await?;
            bucket.num_entries += 1;
        }
        bucket
            .file
            .sync_all()
            .await
            .context("Failed to sync bucket contents")?;

        Ok((bucket.uuid, prev_size))
    }

    pub async fn rotate(&self) -> Result<()> {
        for i in 0..NUM_SHARDS {
            self.rotate_bucket(i).await?;
        }
        Ok(())
    }

    async fn rotate_bucket(&self, i: usize) -> Result<()> {
        let mut bucket = self.buckets[i % NUM_SHARDS].lock().await;

        if bucket.num_entries > 0 {
            log::debug!("Rotating bucket {}", i);
            *bucket = Bucket::new(&self.root)
                .await
                .context("Failed to spawn new bucket")?;
        }

        Ok(())
    }

    async fn lock<'a>(&'a self) -> MutexGuard<'a, Bucket> {
        self.buckets[self.next.fetch_add(1, std::sync::atomic::Ordering::Relaxed) % NUM_SHARDS]
            .lock()
            .await
    }
}
