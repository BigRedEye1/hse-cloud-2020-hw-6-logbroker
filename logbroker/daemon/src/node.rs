use crate::prelude::*;

use crate::api;
use crate::broker;
use crate::clickhouse;
use crate::config;

use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};

#[derive(Clone)]
pub struct Node {
    conf: config::Settings,
    broker: std::sync::Arc<broker::Broker>,
    clickhouse: clickhouse::Client,
}

struct Uploader {
    clickhouse: clickhouse::Client,
}

#[async_trait::async_trait]
impl broker::Sender for Uploader {
    async fn send(&self, entries: Vec<api::LogEntry>) -> Result<()> {
        use std::collections::HashMap;

        let mut entries_by_table: HashMap<String, Vec<api::LogRow>> = HashMap::new();
        for e in entries.into_iter() {
            entries_by_table
                .entry(e.table_name)
                .or_insert(Vec::new())
                .push(e.row);
        }

        for (k, v) in entries_by_table.into_iter() {
            self.send_one_table(&k, v)
                .await
                .context("Failed to send logs to table")?;
        }

        Ok(())
    }
}

impl Uploader {
    async fn send_one_table(&self, table: &str, entries: Vec<api::LogRow>) -> Result<()> {
        let mut json = Vec::new();
        let mut csv = Vec::new();
        let mut rowbinary = Vec::new();

        for e in entries.into_iter() {
            match e {
                api::LogRow::List(list) => csv.push(list),
                api::LogRow::Json(value) => json.push(value),
                api::LogRow::RowBinary(e) => rowbinary.push(e),
            }
        }

        self.send_json(table, json).await?;
        self.send_csv(table, csv).await?;
        self.send_rowbinary(table, rowbinary).await?;

        Ok(())
    }

    async fn send_json(&self, table_name: &str, entries: Vec<serde_json::Value>) -> Result<()> {
        if entries.len() == 0{
            return Ok(());
        }

        let mut body = String::new();
        for e in entries.into_iter() {
            body.push_str(&e.to_string());
            body.push('\n');
        }

        self.send_impl(table_name, "JSONEachRow", body.into_bytes())
            .await
            .context("Failed to send json logs")?;

        Ok(())
    }

    async fn send_csv(&self, table_name: &str, entries: Vec<Vec<serde_json::Value>>) -> Result<()> {
        if entries.len() == 0{
            return Ok(());
        }

        let mut writer = csv::Writer::from_writer(vec![]);

        for e in entries.into_iter() {
            writer.serialize(e)?;
        }

        let body = writer.into_inner()?;

        self.send_impl(table_name, "CSV", body)
            .await
            .context("Failed to send csv logs")?;

        Ok(())
    }

    async fn send_rowbinary(&self, table_name: &str, entries: Vec<Vec<u8>>) -> Result<()> {
        if entries.len() == 0{
            return Ok(());
        }

        let mut body = Vec::new();
        for mut e in entries.into_iter() {
            body.append(&mut e);
        }

        self.send_impl(table_name, "RowBinary", body)
            .await
            .context("Failed to send csv logs")?;

        Ok(())
    }

    async fn send_impl(&self, table: &str, format: &str, body: Vec<u8>) -> Result<String> {
        self.clickhouse
            .query(
                &format!("INSERT INTO {} FORMAT {}", table, format),
                Some(body),
            )
            .await
    }
}

impl Node {
    pub async fn new(conf: config::Settings) -> Result<Self> {
        let mut builder = clickhouse::ClientBuilder::new();

        for replica in conf.ch_replicas.split(',') {
            builder = builder.replica(replica.into());
        }
        if let Some(user) = &conf.ch_user {
            builder = builder.user(user.into());
        }
        if let Some(password) = &conf.ch_password {
            builder = builder.password(password.into());
        }
        let clickhouse = builder.build();

        let broker = std::sync::Arc::new(
            broker::Broker::new(
                &conf,
                Box::new(Uploader {
                    clickhouse: clickhouse.clone(),
                }),
            )
            .await?,
        );

        Ok(Node {
            conf,
            broker,
            clickhouse,
        })
    }

    pub async fn run(&self) -> Result<()> {
        self.run_service().await
    }

    async fn run_service(&self) -> Result<()> {
        log::info!("Starting node service at {}", &self.conf.bind_address);

        let clone = self.clone();
        HttpServer::new(move || {
            App::new()
                .data(clone.clone())
                .service(write_log)
                .service(write_log_rowbinary)
                .service(status)
                .service(healthcheck)
                .service(show_create_table)
        })
        .bind(self.conf.bind_address)?
        .run()
        .await?;

        self.broker.stop().await;

        Ok(())
    }

    async fn write_log(&self, req: api::WriteLogRequest) -> impl Responder {
        match self.process_write_log(req).await {
            Ok(res) => HttpResponse::Ok().json(res),
            Err(e) => {
                log::error!("Write log failed: {:?}", e);
                HttpResponse::ServiceUnavailable().json(api::Error {
                    error: e.to_string(),
                })
            }
        }
    }

    async fn process_write_log(&self, req: api::WriteLogRequest) -> Result<api::WriteLogResponse> {
        let uuid = self.broker.submit(req).await?;
        Ok(api::WriteLogResponse { bucket: uuid })
    }

    async fn write_log_rowbinary(&self, table: String, body: web::Bytes) -> impl Responder {
        match self
            .process_write_log(vec![api::LogEntry {
                table_name: table,
                row: api::LogRow::RowBinary(body.to_vec()),
            }])
            .await
        {
            Ok(res) => HttpResponse::Ok().json(res),
            Err(e) => {
                log::error!("Write log failed: {:?}", e);
                HttpResponse::ServiceUnavailable().json(api::Error {
                    error: e.to_string(),
                })
            }
        }
    }

    async fn status(&self, id: &str) -> impl Responder {
        match self.process_status(id).await {
            Ok(res) => HttpResponse::Ok().json(res),
            Err(e) => {
                log::error!("Status failed: {:?}", e);
                HttpResponse::ServiceUnavailable().json(api::Error {
                    error: e.to_string(),
                })
            }
        }
    }

    async fn process_status(&self, id: &str) -> Result<api::Status> {
        let st = self.broker.status(id).await?;
        Ok(api::Status { status: st })
    }

    async fn show_create_table(&self, table_name: &str) -> impl Responder {
        match self
            .clickhouse
            .query(&format!("SHOW CREATE TABLE \"{}\"", table_name), None)
            .await
        {
            Ok(res) => HttpResponse::Ok()
                .content_type("text/plain; charset=utf-8")
                .body(res.replace("\\n", "\n")),
            Err(e) => HttpResponse::ServiceUnavailable().json(api::Error {
                error: e.to_string(),
            }),
        }
    }
}

#[post("/write_log")]
async fn write_log(node: web::Data<Node>, json: web::Json<api::WriteLogRequest>) -> impl Responder {
    node.write_log(json.0).await
}

#[post("/write_log_rowbinary")]
async fn write_log_rowbinary(
    node: web::Data<Node>,
    web::Query(query): web::Query<api::TableQuery>,
    body: web::Bytes,
) -> impl Responder {
    node.write_log_rowbinary(query.table_name, body).await
}

#[get("/status/{bucket}")]
async fn status(node: web::Data<Node>, bucket: web::Path<String>) -> impl Responder {
    node.status(bucket.as_ref()).await
}

#[get("/healthcheck")]
async fn healthcheck() -> impl Responder {
    HttpResponse::Ok()
}

#[get("/show_create_table")]
async fn show_create_table(
    node: web::Data<Node>,
    web::Query(table_name): web::Query<api::ShowCreateTableRequest>,
) -> impl Responder {
    node.show_create_table(&table_name.table_name).await
}
