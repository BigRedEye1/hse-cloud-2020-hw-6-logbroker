use serde::{Deserialize, Serialize};

pub type WriteLogRequest = Vec<LogEntry>;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "format", content = "rows")]
pub enum LogRow {
    List(Vec<serde_json::Value>),
    Json(serde_json::Value),

    #[serde(with = "serde_bytes")]
    RowBinary(Vec<u8>),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LogEntry {
    #[serde(flatten)]
    pub row: LogRow,
    pub table_name: String,
}

#[derive(Serialize, Debug)]
pub struct WriteLogResponse {
    pub bucket: uuid::Uuid,
}

#[derive(Serialize, Debug)]
pub struct Error {
    pub error: String,
}

#[derive(Serialize, Debug, Clone, Copy, sqlx::Type)]
#[serde(rename_all = "snake_case")]
#[repr(i32)]
pub enum BucketStatus {
    Unknown = 0,
    Pending = 1,
    Failure = 2,
    Success = 3,
}

#[derive(Serialize, Debug)]
pub struct Status {
    pub status: BucketStatus,
}

#[derive(Deserialize)]
pub struct ShowCreateTableRequest {
    pub table_name: String,
}

#[derive(Serialize, Deserialize)]
pub struct TableQuery {
    pub table_name: String,
}
