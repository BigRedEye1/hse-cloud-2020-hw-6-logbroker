use crate::prelude::*;
use crate::api;

use sqlx::sqlite;

#[derive(Clone)]
pub struct Repo {
    pool: sqlx::Pool<sqlx::Sqlite>,
}

fn touch(path: &std::path::Path) -> Result<()> {
    if let Some(parent) = path.parent() {
        std::fs::create_dir_all(parent)?;
    }
    match std::fs::OpenOptions::new()
        .create(true)
        .write(true)
        .open(path)
    {
        Ok(_) => Ok(()),
        Err(e) => Err(e.into()),
    }
}

impl Repo {
    pub async fn new(path: &std::path::Path) -> Result<Repo> {
        touch(path)?;
        let pool = sqlite::SqlitePoolOptions::new()
            .max_connections(4)
            .connect(&format!(
                "sqlite://{}",
                path.to_str().expect("Invalid database path")
            ))
            .await
            .context("Failed to create database")?;

        let repo = Repo { pool };

        repo.migrate().await.context("Failed to migrate db")?;

        Ok(repo)
    }

    async fn migrate(&self) -> Result<()> {
        log::info!("Trying to migrate db");
        let mut delay = std::time::Duration::from_millis(100);
        loop {
            let res = sqlx::migrate!("src/migrations").run(&self.pool).await;

            match res {
                Ok(_) => {
                    log::info!("Successfully applied migrations");
                    break;
                }
                Err(e) => {
                    delay *= 2;
                    log::error!(
                        "Failed to apply migrations: {:?}, sleeping {} seconds",
                        e,
                        delay.as_secs_f32()
                    );
                }
            }

            tokio::time::sleep(delay).await;
        }

        Ok(())
    }

    pub async fn bucket_status(&self, id: &str) -> Result<api::BucketStatus> {
        let res: Option<(api::BucketStatus,)> =
            sqlx::query_as("SELECT status FROM buckets WHERE uuid = ?")
                .bind(id)
                .fetch_optional(&self.pool)
                .await?;

        Ok(res.unwrap_or((api::BucketStatus::Unknown,)).0)
    }

    pub async fn update_status(&self, id: &str, status: api::BucketStatus) -> Result<()> {
        sqlx::query("INSERT INTO buckets(uuid, status) VALUES (?, ?) ON CONFLICT (uuid) DO UPDATE SET status = ?")
            .bind(id)
            .bind(status)
            .bind(status)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    pub async fn erase(&self, id: &str) -> Result<()> {
        sqlx::query("DELETE FROM buckets WHERE uuid = ?")
            .bind(id)
            .execute(&self.pool)
            .await?;

        Ok(())
    }
}
